#------------------------------------------#
# AWS EC2 Configuration
#------------------------------------------#

#---------------Private Subnet-------------#
resource "aws_instance" "Core" {
  count = "1"
  ami = "ami-4836a428"
  instance_type = "m4.4xlarge"
  subnet_id="subnet-9774f9e0"
  security_groups = ["sg-9504f0f1"]
  key_name = "awsdeveloper"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='name=core' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.2 http://rancher.aerisatsp.com/v1/scripts/3FB9012A7353709B78AA:1483142400000:nt9aTqD3nal5ykGoEJlDLzeJMU
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-Dev-Core3"
        Deployment = "Dev"
        Component = "Application"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "NonCore" {
  count = "1"
  ami = "ami-4836a428"
  instance_type = "m4.4xlarge"
  subnet_id="subnet-9774f9e0"
  security_groups = ["sg-9504f0f1"]
  key_name = "awsdeveloper"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='name=noncore' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.2 http://rancher.aerisatsp.com/v1/scripts/3FB9012A7353709B78AA:1483142400000:nt9aTqD3nal5ykGoEJlDLzeJMU
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-Dev-NonCore3"
        Deployment = "Dev"
        Component = "Application"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
#----------------Public Subnet--------------#
resource "aws_instance" "Nginx" {
  count = "1"
  ami = "ami-4836a428"
  instance_type = "t2.medium"
  subnet_id="subnet-300d8047"
  associate_public_ip_address = true
  security_groups = ["sg-865d4be1"]
  key_name = "awsdeveloper"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='name=nginx' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.2 http://rancher.aerisatsp.com/v1/scripts/3FB9012A7353709B78AA:1483142400000:nt9aTqD3nal5ykGoEJlDLzeJMU
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "50"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-Dev-Nginx3"
        Deployment = "Dev"
        Component = "Application"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
