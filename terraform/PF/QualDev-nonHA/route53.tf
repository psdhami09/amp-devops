resource "aws_route53_record" "www-mmc-qualdev" {
   zone_id = "${var.zone_id}"
   name = "${var.record_name}"
   type = "A"
   ttl = "300"
   records = ["${aws_instance.Nginx.0.public_ip}"]
}
