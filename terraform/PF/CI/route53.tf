resource "aws_route53_record" "www-mmc-ci" {
   zone_id = "${var.zone_id}"
   name = "${var.name}"
   type = "A"
   ttl = "300"
   records = ["${aws_instance.Nginx.0.public_ip}"]
}
