#------------------------------------------#
# AWS EC2 Configuration
#------------------------------------------#

#---------------Private Subnet-------------#
resource "aws_instance" "Core" {
  count = "1"
  ami = "ami-6df1e514"
  instance_type = "m4.4xlarge"
  subnet_id="subnet-9774f9e0"
  security_groups = ["sg-9504f0f1"]
  key_name = "awsdeveloper"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=core' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:latest http://rancher.aerisatsp.com/v1/scripts/5934690B035B89D424F9:1483142400000:BftL9Z5ZsHdXmpUUge7NTgjVLZU
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-CI-Core"
        Deployment = "CI"
        Component = "Application"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "NonCore" {
  count = "1"
  ami = "ami-6df1e514"
  instance_type = "m4.4xlarge"
  subnet_id="subnet-9774f9e0"
  security_groups = ["sg-9504f0f1"]
  key_name = "awsdeveloper"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=noncore' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:latest http://rancher.aerisatsp.com/v1/scripts/5934690B035B89D424F9:1483142400000:BftL9Z5ZsHdXmpUUge7NTgjVLZU
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-CI-NonCore"
        Deployment = "CI"
        Component = "Application"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "Kafka" {
  count = "1"
  ami = "ami-6df1e514"
  instance_type = "m4.xlarge"
  subnet_id="subnet-9774f9e0"
  security_groups = ["sg-9504f0f1"]
  key_name = "awsdeveloper"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=kafka' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:latest http://rancher.aerisatsp.com/v1/scripts/5934690B035B89D424F9:1483142400000:BftL9Z5ZsHdXmpUUge7NTgjVLZU
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-CI-Kafka"
        Deployment = "CI"
        Component = "Application"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "MQTT" {
  count = "1"
  ami = "ami-6df1e514"
  instance_type = "m4.xlarge"
  subnet_id="subnet-9774f9e0"
  security_groups = ["sg-9504f0f1"]
  key_name = "awsdeveloper"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=mqtt' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:latest http://rancher.aerisatsp.com/v1/scripts/5934690B035B89D424F9:1483142400000:BftL9Z5ZsHdXmpUUge7NTgjVLZU
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-CI-MQTT"
        Deployment = "CI"
        Component = "Application"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "Redis" {
  count = "1"
  ami = "ami-6df1e514"
  instance_type = "m4.xlarge"
  subnet_id="subnet-9774f9e0"
  security_groups = ["sg-9504f0f1"]
  key_name = "awsdeveloper"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=redis' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:latest http://rancher.aerisatsp.com/v1/scripts/5934690B035B89D424F9:1483142400000:BftL9Z5ZsHdXmpUUge7NTgjVLZU
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-CI-Redis"
        Deployment = "CI"
        Component = "Application"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
#----------------Public Subnet--------------#
resource "aws_instance" "Nginx" {
  count = "1"
  ami = "ami-6df1e514"
  instance_type = "t2.medium"
  subnet_id="subnet-300d8047"
  associate_public_ip_address = true
  security_groups = ["sg-84e95efe"]
  key_name = "awsdeveloper"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=nginx' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:latest http://rancher.aerisatsp.com/v1/scripts/5934690B035B89D424F9:1483142400000:BftL9Z5ZsHdXmpUUge7NTgjVLZU
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "50"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-CI-Nginx"
        Deployment = "CI"
        Component = "Application"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
