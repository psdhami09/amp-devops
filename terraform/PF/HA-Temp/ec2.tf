#------------------------------------------#
# AWS EC2 Configuration
#------------------------------------------#

#---------------Private Subnet-------------#
resource "aws_instance" "Core" {
  count = "2"
  ami = "ami-aa5ebdd2"
  instance_type = "m4.2xlarge"
  subnet_id="subnet-96f050cf"
  security_groups = ["sg-3bb13d41"]
  key_name = "ampdeveloper"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker-1.12.6
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=core' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.5 http://rancher.aerisatsp.com/v1/scripts/B494BB0D6C5B68489ED2:1483142400000:fXlxITRXkTD6EYeaWUGITX7PmTM
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-HA-Temp-Core"
        Deployment = "QA"
        Component = "Application"
        Project = "AMP"
		Owner = "vikram.rawat@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "NonCore" {
  count = "2"
  ami = "ami-aa5ebdd2"
  instance_type = "m4.2xlarge"
  subnet_id="subnet-96f050cf"
  security_groups = ["sg-3bb13d41"]
  key_name = "ampdeveloper"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker-1.12.6
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=noncore' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.5 http://rancher.aerisatsp.com/v1/scripts/B494BB0D6C5B68489ED2:1483142400000:fXlxITRXkTD6EYeaWUGITX7PmTM
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-HA-Temp-NonCore"
        Deployment = "QA"
        Component = "Application"
        Project = "AMP"
		Owner = "vikram.rawat@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "Kafka" {
  count = "3"
  ami = "ami-aa5ebdd2"
  instance_type = "m4.xlarge"
  subnet_id="subnet-96f050cf"
  security_groups = ["sg-3bb13d41"]
  key_name = "automotive-devops"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker-1.12.6
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=kafka' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.5 http://rancher.aerisatsp.com/v1/scripts/B494BB0D6C5B68489ED2:1483142400000:fXlxITRXkTD6EYeaWUGITX7PmTM
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-HA-Temp-Kafka"
        Deployment = "QA"
        Component = "Application"
        Project = "AMP"
		Owner = "vikram.rawat@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "MQTT" {
  count = "3"
  ami = "ami-aa5ebdd2"
  instance_type = "m4.xlarge"
  subnet_id="subnet-96f050cf"
  security_groups = ["sg-3bb13d41"]
  key_name = "automotive-devops"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker-1.12.6
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=mqtt' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.5 http://rancher.aerisatsp.com/v1/scripts/B494BB0D6C5B68489ED2:1483142400000:fXlxITRXkTD6EYeaWUGITX7PmTM
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-HA-Temp-MQTT"
        Deployment = "QA"
        Component = "Application"
        Project = "AMP"
		Owner = "vikram.rawat@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "Redis" {
  count = "3"
  ami = "ami-aa5ebdd2"
  instance_type = "m4.xlarge"
  subnet_id="subnet-96f050cf"
  security_groups = ["sg-3bb13d41"]
  key_name = "automotive-devops"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker-1.12.6
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=redis' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.5 http://rancher.aerisatsp.com/v1/scripts/B494BB0D6C5B68489ED2:1483142400000:fXlxITRXkTD6EYeaWUGITX7PmTM
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-HA-Temp-Redis"
        Deployment = "QA"
        Component = "Application"
        Project = "AMP"
		Owner = "vikram.rawat@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "ELK" {
  count = "2"
  ami = "ami-aa5ebdd2"
  instance_type = "m4.xlarge"
  subnet_id="subnet-96f050cf"
  security_groups = ["sg-3bb13d41"]
  key_name = "automotive-devops"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker-1.12.6
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=elk' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.5 http://rancher.aerisatsp.com/v1/scripts/B494BB0D6C5B68489ED2:1483142400000:fXlxITRXkTD6EYeaWUGITX7PmTM
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-HA-Temp-ELK"
        Deployment = "QA"
        Component = "Application"
        Project = "AMP"
		Owner = "vikram.rawat@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}

#----------------Public Subnet--------------#
resource "aws_instance" "Nginx" {
  count = "2"
  ami = "ami-aa5ebdd2"
  instance_type = "c4.xlarge"
  subnet_id="subnet-300d8047"
  associate_public_ip_address = true
  security_groups = ["sg-5448c42e"]
  key_name = "automotive-devops"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker-1.12.6
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=nginx' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.5 http://rancher.aerisatsp.com/v1/scripts/B494BB0D6C5B68489ED2:1483142400000:fXlxITRXkTD6EYeaWUGITX7PmTM
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "50"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-HA-Temp-Nginx"
        Deployment = "QA"
        Component = "Application"
        Project = "AMP"
		Owner = "vikram.rawat@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
