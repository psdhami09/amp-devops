#------------------------------------------#
# AWS EC2 Configuration
#------------------------------------------#

#---------------Private Subnet-------------#
resource "aws_instance" "Core" {
  count = "2"
  ami = "ami-e689729e"
  instance_type = "m4.2xlarge"
  subnet_id="subnet-43472424"
  security_groups = ["sg-684eb913"]
  key_name = "PF-PreProd-Internal"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      100000\nroot      hard    nofile      500000\nroot      soft    nofile      100000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-33`
	          #Installing Docker
              sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=core' --rm --privileged -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.5 http://rancher.aerpf.com/v1/scripts/CF02DC45D97403E98D67:1483142400000:WRP8Ww0iJgy8gZCPFqbrYNViA
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "PF-PreProd-Core"
        Deployment = "PreProduction"
        Component = "Application"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "NonCore" {
  count = "2"
  ami = "ami-e689729e"
  instance_type = "m4.2xlarge"
  subnet_id="subnet-43472424"
  security_groups = ["sg-684eb913"]
  key_name = "PF-PreProd-Internal"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      100000\nroot      hard    nofile      500000\nroot      soft    nofile      100000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-33`
	          #Installing Docker
              sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=noncore' --rm --privileged -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.5 http://rancher.aerpf.com/v1/scripts/CF02DC45D97403E98D67:1483142400000:WRP8Ww0iJgy8gZCPFqbrYNViA
              EOF
              
  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "PF-PreProd-NonCore"
        Deployment = "PreProduction"
        Component = "Application"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "Kafka" {
  count = "2"
  ami = "ami-e689729e"
  instance_type = "m4.2xlarge"
  subnet_id="subnet-43472424"
  security_groups = ["sg-684eb913"]
  key_name = "PF-PreProd-Internal"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      100000\nroot      hard    nofile      500000\nroot      soft    nofile      100000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-33`
	          #Installing Docker
              sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=kafka' --rm --privileged -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.5 http://rancher.aerpf.com/v1/scripts/CF02DC45D97403E98D67:1483142400000:WRP8Ww0iJgy8gZCPFqbrYNViA
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    delete_on_termination = "true"
   }
      tags {
        Name = "PF-PreProd-Kafka"
        Deployment = "PreProduction"
        Component = "Kafka"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "MQTT" {
  count = "2"
  ami = "ami-e689729e"
  instance_type = "m4.2xlarge"
  subnet_id="subnet-43472424"
  security_groups = ["sg-684eb913"]
  key_name = "PF-PreProd-Internal"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      100000\nroot      hard    nofile      500000\nroot      soft    nofile      100000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-33`
	          #Installing Docker
              sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=mqtt' --rm --privileged -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.5 http://rancher.aerpf.com/v1/scripts/CF02DC45D97403E98D67:1483142400000:WRP8Ww0iJgy8gZCPFqbrYNViA
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    delete_on_termination = "true"
   }
      tags {
        Name = "PF-PreProd-MQTT"
        Deployment = "PreProduction"
        Component = "MQTT"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "Redis" {
  count = "2"
  ami = "ami-e689729e"
  instance_type = "m4.xlarge"
  subnet_id="subnet-d5aecdb2"
  security_groups = ["sg-684eb913"]
  key_name = "PF-PreProd-Internal"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      100000\nroot      hard    nofile      500000\nroot      soft    nofile      100000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-33`
	          #Installing Docker
              sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=redis' --rm --privileged -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.5 http://rancher.aerpf.com/v1/scripts/CF02DC45D97403E98D67:1483142400000:WRP8Ww0iJgy8gZCPFqbrYNViA
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    delete_on_termination = "true"
   }
      tags {
        Name = "PF-PreProd-Redis"
        Deployment = "PreProduction"
        Component = "Redis"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "ELK" {
  count = "2"
  ami = "ami-e689729e"
  instance_type = "m4.xlarge"
  subnet_id="subnet-43472424"
  security_groups = ["sg-684eb913"]
  key_name = "PF-PreProd-Internal"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      100000\nroot      hard    nofile      500000\nroot      soft    nofile      100000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-33`
	          #Installing Docker
              sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=elk' --rm --privileged -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.5 http://rancher.aerpf.com/v1/scripts/CF02DC45D97403E98D67:1483142400000:WRP8Ww0iJgy8gZCPFqbrYNViA
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "PF-PreProd-ELK"
        Deployment = "PreProduction"
        Component = "ELK"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "Analytics" {
  count = "2"
  ami = "ami-e689729e"
  instance_type = "m4.xlarge"
  subnet_id="subnet-43472424"
  security_groups = ["sg-684eb913"]
  key_name = "PF-PreProd-Internal"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      100000\nroot      hard    nofile      500000\nroot      soft    nofile      100000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-33`
	          #Installing Docker
              sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=analytics' --rm --privileged -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.5 http://rancher.aerpf.com/v1/scripts/CF02DC45D97403E98D67:1483142400000:WRP8Ww0iJgy8gZCPFqbrYNViA
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    delete_on_termination = "true"
   }
      tags {
        Name = "PF-PreProd-Analytics"
        Deployment = "PreProduction"
        Component = "Analytics"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "Monitoring" {
  count = "2"
  ami = "ami-e689729e"
  instance_type = "m4.xlarge"
  subnet_id="subnet-43472424"
  security_groups = ["sg-684eb913"]
  key_name = "PF-PreProd-Internal"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      100000\nroot      hard    nofile      500000\nroot      soft    nofile      100000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-33`
	          #Installing Docker
              sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=monitoring' --rm --privileged -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.5 http://rancher.aerpf.com/v1/scripts/CF02DC45D97403E98D67:1483142400000:WRP8Ww0iJgy8gZCPFqbrYNViA
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    delete_on_termination = "true"
   }
      tags {
        Name = "PF-PreProd-Monitoring"
        Deployment = "PreProduction"
        Component = "Monitoring"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}

#----------------Public Subnet--------------#
resource "aws_instance" "Nginx" {
  count = "2"
  ami = "ami-04eb1b64"
  instance_type = "c4.xlarge"
  subnet_id="subnet-f7462590"
  associate_public_ip_address = true
  security_groups = ["sg-5b9d6b20"]
  key_name = "PF-PreProd-External"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      300000\nroot      hard    nofile      500000\nroot      soft    nofile      300000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-33`
	          #Installing Docker
              sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=nginx' --rm --privileged -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.5 http://rancher.aerpf.com/v1/scripts/CF02DC45D97403E98D67:1483142400000:WRP8Ww0iJgy8gZCPFqbrYNViA
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "50"
    delete_on_termination = "true"
   }
      tags {
        Name = "PF-PreProd-Nginx"
        Deployment = "PreProduction"
        Component = "Nginx"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "Nginxplus" {
  count = "2"
  ami = "ami-04eb1b64"
  instance_type = "c4.xlarge"
  subnet_id="subnet-f7462590"
  associate_public_ip_address = true
  security_groups = ["sg-5b9d6b20"]
  key_name = "PF-PreProd-External"

  user_data = <<-EOF
              #!/bin/bash
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      300000\nroot      hard    nofile      500000\nroot      soft    nofile      300000" >> /etc/security/limits.conf
			  EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "50"
    delete_on_termination = "true"
   }
      tags {
        Name = "PF-PreProd-Nginxplus"
        Deployment = "PreProduction"
        Component = "Nginxplus"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}