resource "aws_route53_record" "www-pf-preprod" {
   zone_id = "${var.zone_id}"
   name = "${var.route53record_name}"
   type = "A"
   ttl = "300"
   records = ["35.165.131.33"]
}
