#------------------------------------------#
# AWS EC2 Configuration
#------------------------------------------#

#---------------Private Subnet-------------#
resource "aws_instance" "Core" {
  count = "2"
  ami = "ami-5ec1673e"
  instance_type = "r3.2xlarge"
  subnet_id="subnet-d64320b1"
  security_groups = ["sg-684eb913"]
  key_name = "MMC-Prod-Internal"

  user_data = <<-EOF
              #!/bin/bash
              #Update pakage
              sudo yum update -y
              #Installing Docker
              sudo yum install -y docker
              sudo service docker start
              sleep 3
              #install rancher agent
              sudo docker run -d --privileged -e CATTLE_HOST_LABELS='services=core' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.1 http://rancher.aerpf.com:8080/v1/scripts/B06F6F547F7C3A1014CF:1483142400000:xM3cElbvqodS6vPXRZKtn8n3AA
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-Prod-Core"
        Deployment = "Production"
        Component = "Apllication"
        Project = "AMP"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "NonCore" {
  count = "2"
  ami = "ami-5ec1673e"
  instance_type = "r3.2xlarge"
  subnet_id="subnet-d64320b1"
  security_groups = ["sg-684eb913"]
  key_name = "MMC-Prod-Internal"

  user_data = <<-EOF
              #!/bin/bash
              #Update pakage
              sudo yum update -y
              #Installing Docker
              sudo yum install -y docker
              sudo service docker start
              sleep 3
              #install rancher agent
              sudo docker run -d --privileged -e CATTLE_HOST_LABELS='services=noncore' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.1 http://rancher.aerpf.com:8080/v1/scripts/B06F6F547F7C3A1014CF:1483142400000:xM3cElbvqodS6vPXRZKtn8n3AA
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-Prod-NonCore"
        Deployment = "Production"
        Component = "Application"
        Project = "AMP"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "Kafka" {
  count = "3"
  ami = "ami-5ec1673e"
  instance_type = "m4.xlarge"
  subnet_id="subnet-d64320b1"
  security_groups = ["sg-684eb913"]
  key_name = "MMC-Prod-Internal"

  user_data = <<-EOF
              #!/bin/bash
              #Update pakage
              sudo yum update -y
              #Installing Docker
              sudo yum install -y docker
              sudo service docker start
              sleep 3
              #install rancher agent
              sudo docker run -d --privileged -e CATTLE_HOST_LABELS='services=kafka' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.1 http://rancher.aerpf.com:8080/v1/scripts/B06F6F547F7C3A1014CF:1483142400000:xM3cElbvqodS6vPXRZKtn8n3AA
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-Prod-Kafka"
        Deployment = "Production"
        Component = "Kafka"
        Project = "AMP"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "MQTT" {
  count = "3"
  ami = "ami-5ec1673e"
  instance_type = "m4.xlarge"
  subnet_id="subnet-d64320b1"
  security_groups = ["sg-684eb913"]
  key_name = "MMC-Prod-Internal"

  user_data = <<-EOF
              #!/bin/bash
              #Update pakage
              sudo yum update -y
              #Installing Docker
              sudo yum install -y docker
              sudo service docker start
              sleep 3
              #install rancher agent
              sudo docker run -d --privileged -e CATTLE_HOST_LABELS='services=mqtt' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.1 http://rancher.aerpf.com:8080/v1/scripts/B06F6F547F7C3A1014CF:1483142400000:xM3cElbvqodS6vPXRZKtn8n3AA
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-Prod-MQTT"
        Deployment = "Production"
        Component = "RabbitMQ"
        Project = "AMP"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "Redis" {
  count = "3"
  ami = "ami-5ec1673e"
  instance_type = "m4.xlarge"
  subnet_id="subnet-7fadce18"
  security_groups = ["sg-684eb913"]
  key_name = "MMC-Prod-Internal"

  user_data = <<-EOF
              #!/bin/bash
              #Update pakage
              sudo yum update -y
              #Installing Docker
              sudo yum install -y docker
              sudo service docker start
              sleep 3
              #install rancher agent
              sudo docker run -d --privileged -e CATTLE_HOST_LABELS='services=redis' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.1 http://rancher.aerpf.com:8080/v1/scripts/B06F6F547F7C3A1014CF:1483142400000:xM3cElbvqodS6vPXRZKtn8n3AA
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-Prod-Redis"
        Deployment = "Production"
        Component = "Redis"
        Project = "AMP"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "ELK" {
  count = "2"
  ami = "ami-5ec1673e"
  instance_type = "m4.xlarge"
  subnet_id="subnet-d64320b1"
  security_groups = ["sg-684eb913"]
  key_name = "MMC-Prod-Internal"

  user_data = <<-EOF
              #!/bin/bash
              #Update pakage
              sudo yum update -y
              #Installing Docker
              sudo yum install -y docker
              sudo service docker start
              sleep 3
              #install rancher agent
              sudo docker run -d --privileged -e CATTLE_HOST_LABELS='services=elk' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.1 http://rancher.aerpf.com:8080/v1/scripts/B06F6F547F7C3A1014CF:1483142400000:xM3cElbvqodS6vPXRZKtn8n3AA
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-Prod-ELK"
        Deployment = "Production"
        Component = "ELK"
        Project = "AMP"
    }
  lifecycle {
    create_before_destroy = true
  }
}
#----------------Public Subnet--------------#
resource "aws_instance" "Nginx" {
  count = "2"
  ami = "ami-5ec1673e"
  instance_type = "m4.xlarge"
  subnet_id="subnet-f7462590"
  associate_public_ip_address = true
  security_groups = ["sg-5b9d6b20"]
  key_name = "MMC-Prod-Nginx"

  user_data = <<-EOF
              #!/bin/bash
              #Update pakage
              sudo yum update -y
              #Installing Docker
              sudo yum install -y docker
              sudo service docker start
              sleep 3
              #install rancher agent
              sudo docker run -d --privileged -e CATTLE_HOST_LABELS='services=nginx' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.1 http://rancher.aerpf.com:8080/v1/scripts/B06F6F547F7C3A1014CF:1483142400000:xM3cElbvqodS6vPXRZKtn8n3AA
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-Prod-Nginx"
        Deployment = "Production"
        Component = "Nginx"
        Project = "AMP"
    }
  lifecycle {
    create_before_destroy = true
  }
}
