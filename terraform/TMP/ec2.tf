#------------------------------------------#
# AWS EC2 Configuration
#------------------------------------------#

#---------------Private Subnet-------------#
resource "aws_instance" "Core" {
  count = "1"
  ami = "ami-6df1e514"
  instance_type = "m4.2xlarge"
  subnet_id="subnet-9774f9e0"
  security_groups = ["sg-9504f0f1"]
  key_name = "ampdeveloper"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=core' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:latest http://rancher.aerisatsp.com/v1/scripts/B1477D7CE537CA3311D6:1483142400000:OvHHzVWylGnTk6KmA0imN2Myv8
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-Qualdev-Core"
        Deployment = "QA"
        Component = "Application"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "NonCore" {
  count = "1"
  ami = "ami-6df1e514"
  instance_type = "m4.2xlarge"
  subnet_id="subnet-9774f9e0"
  security_groups = ["sg-9504f0f1"]
  key_name = "ampdeveloper"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=noncore' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.2 http://rancher.aerisatsp.com/v1/scripts/B1477D7CE537CA3311D6:1483142400000:OvHHzVWylGnTk6KmA0imN2Myv8
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-Qualdev-NonCore"
        Deployment = "QA"
        Component = "Application"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "Monitoring" {
  count = "1"
  ami = "ami-6df1e514"
  instance_type = "t2.medium"
  subnet_id="subnet-9774f9e0"
  security_groups = ["sg-9504f0f1"]
  key_name = "ampdeveloper"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=monitoring' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.2 http://rancher.aerisatsp.com/v1/scripts/B1477D7CE537CA3311D6:1483142400000:OvHHzVWylGnTk6KmA0imN2Myv8
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-Qualdev-Monitoring"
        Deployment = "QA"
        Component = "Application"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
#----------------Public Subnet--------------#
resource "aws_instance" "Nginx" {
  count = "1"
  ami = "ami-6df1e514"
  instance_type = "t2.small"
  subnet_id="subnet-300d8047"
  associate_public_ip_address = true
  security_groups = ["sg-865d4be1"]
  key_name = "automotive-devops"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=nginx' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.2 http://rancher.aerisatsp.com/v1/scripts/B1477D7CE537CA3311D6:1483142400000:OvHHzVWylGnTk6KmA0imN2Myv8
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "50"
    delete_on_termination = "true"
   }
      tags {
        Name = "MMC-Qualdev-Nginx"
        Deployment = "QA"
        Component = "Application"
        Project = "AMP"
		Owner = "pritpal.singh@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
