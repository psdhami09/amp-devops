Change the following ebus default parameters, if you need:

The following environment variables are passed to confd with default values in order to set up RabbitMQ's configuration file, change them accordinly, if you need::

•	Partition handling=autoheal #RabbitMQ's cluster handling setting

•	Erlang cookie=defaultcookie #cookie to allow nodes communication

•	Net ticktime:60 #adjusts the frequency of both tick messages and detection of failures

•	Confd args=--interval 5 #additional confd args

•	Volume driver=local #type of volume driver

•	Initial data nodes=3 #scale of ebus containers



You can pass an alternate `confd` configuration via the `ALTERNATE_CONF` environment variable.